# Polytech Annecy

## Sommaire
Rapport
*  Objectifs
*  Qu'est-ce que ce projet a de Data Science ?
*  État d'avancement
*  Description technique
*  Suites possibles
*  Vidéo de présentation (cf. fichier vidéo sur le dépôt)

## Objectifs : Visualisation des déplacements avant le 20ème siècle (Sujet d'Ilham Alloui)
Au 21ème siècle, il est fréquent de s'éloigner de sa ville de naissance pour l'éducation ou pour le travail. Mais ceci était moins fréquent avant le 21ème siècle. 
L'objectif de ce projet est d'offrir une cartographie dynamique des déplacements d'un (ou plusieurs) nom de famille sur une période de temps. On remarque que les déplacements se font généralement de proche en proche : l'époux ou l'épouse qui change de commune au moment du mariage, permettant ainsi d'essaimer.
L'acquisition de données se fera par l'intermédiaire des sources généalogiques (particulièrement geneanet.org). Un travail important est à réaliser sur l'acquisition.

## Qu'est-ce que ce projet a de Data Science ?
Ce projet réalise presque tous les traitements possibles sur une donnée :
*  Récupération ("aspirateur de site")
*  Traitement (rendre les données récoltées utilisable pour le projet)
*  Stockage (gestion de fichiers CSV)
*  Affichage / Visualisation (rendre lisible les données)
*  Analyse (utiliser ces données)

## État d'avancement
Chaque période correspond à un temps dédié au projet, et a duré non loin d'une journée complète.

Période 1 : Récupération des données (geneanet.org)
*  Nom
*  Sexe
*  AnnéeNaissance
*  AnnéeMort
*  Location

Période 2 : Récupération des données (geo.api.gouv.fr)
*  Ville
*  CodePostal
*  Longitude
*  Latitude

Période 3 : Présentation
*  Affichage sur carte des données brutes (latitude & longitude)
*  Fonctionnant sur un système de fichier CSV, réexploitable
*  Mise en forme du fichier de présentation

Période 4 : Exécution par shell
*  help
*  get (création options limiteur de données récupérées + mode rapide sans latitude & longitude)
*  display (créations option sélecteur d'année + maniement dynamique de la carte)
*  Ouverture de la visibilité du projet sur Internet

Période 5 : Prise en compte des retours du client
* analyze
* Bibliothèques en début de fichier
* Réalisation de ce rapport
* Réalisation de la vidéo

## Suites possibles
* Transformer en bibliothèque Python
* Documentation & User guide
* Améliorer les interactions dynamiques de la carte
* Interface graphique de requêtage (fenêtre)