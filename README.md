# GeneanetAPI
API de récupération de données, et d'affichage sur carte

# Dépendances nécessaires
## Libraries natives
* collections (OrderedDict)
* time (time)
* os (remove)
* requests (get)
* sys (argv)
* json (loads)

## Libraries complémentaires
Ces librairies sont à installer, et peuvent aussi avoir des dépendances.
* [geoplotlib](https://github.com/andrea-cuttone/geoplotlib)
* [bs4 - Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

# Comment fonctionne-t-il ?
Télécharger les 2 fichiers python, et exécutez la commande suivante :
```
python3 geneanetAPI.py --help
```
Vous aurez à cet endroit toutes les fonctionnalités que vous pouvez réaliser en commandes bash.