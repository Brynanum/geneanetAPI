from json import loads
from requests import get
from bs4 import BeautifulSoup

class GeneanetAPIPerson:
    nom=None
    sexe=None
    anneeNaissance=0
    anneeMort=0
    location=None
    ville=None
    codePostal=None
    latitude=None
    longitude=None

    def __init__(self,person,addGeolocalization=True):
        # Nom
        self.nom=person.find('span','fake-a').text.strip().replace('                ',' ')
        # Sexe(True=Homme;False=Femme)
        self.sexe=str(person.find('span','icon-search-femme')==None)
        # AnnéeNaissance & AnnéeMort
        years=person.find('span','text-for-mobile').find_parent().text.split()
        if len(years)==5 and years[2].isdigit() and years[4].isdigit():
            self.anneeNaissance=years[2]
            self.anneeMort=years[4]
        # Location
        self.location=person.find('span','text-for-mobile').find_parent().find_next_sibling().text.strip().replace(',',';')
        # Ville
        location=self.location.split(';')
        self.ville=location[0].strip()
        # CodePostal
        if location[1].strip().isdigit():
            self.codePostal=location[1].strip()
        # Longitude & Latitude
        if addGeolocalization:
            geoApiGouvFr=['https://geo.api.gouv.fr/communes?format=geojson&nom='+self.ville]
            if self.codePostal!=None:
                geoApiGouvFr=['https://geo.api.gouv.fr/communes?format=geojson&codePostal='+self.codePostal+'&nom='+self.ville,'https://geo.api.gouv.fr/communes?format=geojson&codePostal='+self.codePostal,'https://geo.api.gouv.fr/communes?format=geojson&nom='+self.ville]
            for url in geoApiGouvFr:
                geolocalisation=loads(get(url).text)['features']
                if len(geolocalisation)>0:
                    self.longitude=str(geolocalisation[0]['geometry']['coordinates'][0])
                    self.latitude=str(geolocalisation[0]['geometry']['coordinates'][1])
                    break

    def __str__(self):
        result=''
        for value in [self.nom,self.sexe,self.anneeNaissance,self.anneeMort,self.location,self.ville,self.codePostal,self.latitude,self.longitude]:
            if value!=None:
                result+=str(value)
            result+=','
        return result[:-1]