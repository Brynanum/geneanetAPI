from collections import OrderedDict
from os import remove
from requests import get
from sys import argv
from time import sleep

from bs4 import BeautifulSoup
import geoplotlib
from geoplotlib.utils import read_csv

from geneanetAPIPerson import GeneanetAPIPerson

class GeneanetAPI:
    # ==============================================================================
    def get(familyName,pageNumberMax=0,addGeolocalization=True):
        pageNumberNoLimit=(pageNumberMax==0)
        if pageNumberNoLimit:
            print('Détermination du nombre de pages existantes...',end='\r')
            pageNumberMax=int(BeautifulSoup(get('https://www.geneanet.org/fonds/individus/?nom='+familyName+'&size=50&go=1').text,'html.parser').find('ul','pagination').find('li','arrow').find_previous_sibling().a.text)
            print(str(pageNumberMax)+' pages détectées (environ '+str(pageNumberMax*50)+' personnes détectées).\nIl sera possible d\'arrêter le processus toutes les 50 personnes environ.')
        print('Les données seront dans "Geneanet - Data - '+familyName+'.csv".')

        csv=open('Geneanet - Data - '+familyName+'.csv','w')
        csv.write('Nom,Sexe(True=Homme;False=Femme),AnnéeNaissance,AnnéeMort,Location,Ville,CodePostal,lat,lon\n')
        countCollectedData=0
        people=[]
        for pageNumber in range(1,pageNumberMax):
            for person in BeautifulSoup(get('https://www.geneanet.org/fonds/individus/?nom='+familyName+'&page='+str(pageNumber)+'&size=50&go=1').text,'html.parser').find(id="table-resultats").find_all('a') :
                try:
                    people.append(GeneanetAPIPerson(person,addGeolocalization))
                    csv.write(str(people[-1])+'\n')
                    countCollectedData+=1
                    print('Nombre de personnes collectées : '+str(countCollectedData)+'/'+str(pageNumberMax*50),end='\r')
                except :
                    pass
            if pageNumberNoLimit:
                print('Tuez le processus (Ctrl+C) pour s\'arrêter, ou attendez 5 secondes pour continuer.',end='\r')
                sleep(5)
                print('                                                                                  ',end='\r')
        csv.close()
        return people
    # ==============================================================================
    def display(csvPath,year=None):
        # ==== Getting displayable data (with longitude and latitude)
        csvData=open(csvPath,'r')
        csvTemp=open('temp.csv','w')
        data=iter(csvData)
        csvTemp.write(next(data))
        for line in data:
            if line.strip()[-1]!=',':
                if not year:
                    csvTemp.write(line)
                else:
                    periodLife=line.split(',')[2:4]
                    if year>int(periodLife[0]) and year<int(periodLife[1]):
                         csvTemp.write(line)
        csvTemp.close()
        csvData.close()
        
        # ==== Display these data on a map
        data=read_csv('temp.csv')
        geoplotlib.dot(data)
        geoplotlib.show()
        remove('temp.csv')
    # ==============================================================================
    def analyze(csvPath,year=None):
        data={
            'gender':{
                'male':0,
                'female':0
            },
            'yearBirth':{},
            'yearDeath':{}
        }
        csv=open(csvPath,'r')
        csvData=iter(csv)
        next(csvData)
        for line in csvData:
            if year:
                periodLife=line.split(',')[2:4]
                if year<int(periodLife[0]) or year>int(periodLife[1]):
                    continue
            person=line.split(',')
            # Gender
            if person[1]=='True':
                data['gender']['male']+=1
            else:
                data['gender']['female']+=1
            # YearBirth
            if person[2] not in data['yearBirth']:
                data['yearBirth'][person[2]]=0
            data['yearBirth'][person[2]]+=1
            # YearDeath
            if person[3] not in data['yearDeath']:
                data['yearDeath'][person[3]]=0
            data['yearDeath'][person[3]]+=1
        data['yearBirth']=OrderedDict(sorted(data['yearBirth'].items()))
        data['yearDeath']=OrderedDict(sorted(data['yearDeath'].items()))
        csv.close()
        print(data)
        return data

# ==== Shell part ======================================================
if __name__=='__main__':
    if argv[1]=='--display':
        if len(argv)==3:
            GeneanetAPI.display(argv[2])
        elif len(argv)==4:
            GeneanetAPI.display(argv[2],int(argv[3]))
    elif argv[1]=='--analyze':
        if len(argv)==3:
            GeneanetAPI.analyze(argv[2])
        elif len(argv)==4:
            GeneanetAPI.analyze(argv[2],int(argv[3]))
    elif argv[1]=='--get':
        if len(argv)==3:
            GeneanetAPI.get(argv[2])
        elif len(argv)==4:
            GeneanetAPI.get(argv[2],int(argv[3]))
        elif len(argv)==5:
            GeneanetAPI.get(argv[2],int(argv[3]),argv[4].lower() in ['true','t','y','1'])
    elif argv[1]=='--help':
        print("""
GeneanetMap - Récupération et génération de cartes depuis geneanet.org
Créé par Bryan Fauquembergue

3 options possibles :
	--help
		Affiche cette aide
	--get [nom de famille] <nombre pages> <ajout géolocalisation>
		Récupère les données de geneanet.org pour le nom spécifié (fichier CSV)\n
		[nom de famille] : String correspondant au nom recherché
		<nombre pages> : Nombre de paquets d'environ 50 personnes récupérées. Si 0 (valeur par défaut), le maximum est récupéré.
		<ajout géolocalisation> : Booléen de récupération de la longitude et latitude (contre temps d'exécution). Valeur par défaut : True.
	--analyze [chemin vers fichier CSV] <année>
		Extrait quelques statistiques du fichier
		<année> : Affiche les informations liés à l'année spécifiée uniquement
	--display [chemin vers fichier CSV] <année>
		Affiche les données du fichier CSV sur une carte (en fonction des colonnes "lon" et "lat")
		<année> : Affiche les informations liés à l'année spécifiée uniquement
		Touches de clavier utilisable sur la carte interactive :
			P: Prendre une capture d\'écran (qui est sauvegardé dans le répertoire courant)
			M: Afficher / Cacher la carte de fond
			L: Afficher / Cacher les données
			I/O: Agrandir (zoom In) / Rétrécir (zoom Out)
			A/D: Aller à gauche / droite
			W/S: Aller en haut / bas
    	""")
    else:
        print('ERREUR : Cette commande n\'existe  pas.\n Exécutez --help pour obtenir de l\'aide.')
    